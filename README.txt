 BBC News Trainee Test Article Ranker 


This site requests articles in JSON using the BBC articles data set provided at https://github.com/bbc/news-coding-test-dataset for their Article Ranker test. 

It displays these articles on a webpage and after viewing each article, the user can rank them. The webpage uses JQuery and JQuery UI along with HTML and CSS with Bootstrap. 

The ranking would be sent via a POST request to the server, but there is no server, so a POST request is simulated. 




